from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm
# Create your views here.

def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolists": todolists,
    }
    return render(request, "todo_lists/list.html", context)

def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "todo_lists/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list=form.save()
            return redirect("todo_list_detail", id=list.id) 
        
    else:
        form=TodoListForm()
    context={
        "form": form,
    }

    return render(request, "todo_lists/create.html", context)

def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list=form.save()
            return redirect("todo_list_detail", id=list.id) 
        
    else:
        form=TodoListForm(instance=list)
    context={
        "form": form,
    }

    return render(request, "todo_lists/update.html", context)

def todo_list_delete(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todo_lists/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id) 
    
    else:
        form=TodoItemForm()
    context={
        "form": form,
    }
    return render(request, "todo_items/create.html", context)

def todo_item_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list)
        if form.is_valid():
            list=form.save()
            return redirect("todo_list_detail", id=list.id) 
        
    else:
        form=TodoItemForm(instance=list)
    context={
        "form": form,
    }

    return render(request, "todo_items/update.html", context)